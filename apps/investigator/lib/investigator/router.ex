defmodule Investigator.Router do
  use Plug.Router

  alias Investigator.OAuth
  alias Investigator.Store

  plug(Plug.Logger)
  plug(:match)
  plug(:dispatch)

  get "/auth/success" do
    conn = fetch_query_params(conn)

    %{"code" => code, "state" => state} = conn.params

    {status, resp} =
      case OAuth.extract_state(state) do
        {:ok, user_id, auth_token} ->
          case Store.check_auth_token(auth_token, user_id) do
            {:ok, true} ->
              client = OAuth.get_token!(code: code)
              Store.set(user_id, client.token.access_token)
              {200, "Scaffold bot successfully authorized"}

            {:error, :not_founded} ->
              {401, "Unauthorized request"}
          end

        {:error, _} ->
          {401, "Unauthorized request"}
      end

    conn
    |> put_resp_content_type("text/plain")
    |> send_resp(status, resp)
  end

  match _ do
    send_resp(conn, 404, "oops")
  end
end
