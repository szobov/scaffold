defmodule Investigator.Store do
  use GenServer

  def start_link(_) do
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(:ok) do
    {:ok, dets_storage} =
      :dets.open_file(
        :investigator_storage,
        [{:type, :set}, {:ram_file, true}]
      )

    {:ok, %{:storage => dets_storage}}
  end

  def handle_call({:get, user_id}, _from, %{:storage => storage} = state) do
    case :dets.lookup(storage, user_id) do
      [{^user_id, token}] -> {:reply, {:ok, token}, state}
      _ -> {:reply, {:error, :not_founded}, state}
    end
  end

  def handle_cast({:set, user_id, token}, %{:storage => storage} = state) do
    :dets.insert(storage, {user_id, token})
    {:noreply, state}
  end

  def handle_cast({:set_auth_token, token, user_id}, %{:storage => storage} = state) do
    :dets.insert(storage, {token, user_id})
    {:noreply, state}
  end

  def handle_call({:check_auth_token, auth_token, user_id}, _from, %{:storage => storage} = state) do
    case :dets.lookup(storage, auth_token) do
      [{^auth_token, ^user_id}] ->
        :dets.delete(storage, auth_token)
        {:reply, {:ok, true}, state}

      _ ->
        {:reply, {:error, :not_founded}, state}
    end
  end

  def get(user_id) do
    GenServer.call(__MODULE__, {:get, user_id})
  end

  def set(user_id, token) do
    GenServer.cast(__MODULE__, {:set, user_id, token})
  end

  def set_auth_token(auth_token, user_id) do
    GenServer.cast(__MODULE__, {:set_auth_token, auth_token, user_id})
  end

  def check_auth_token(auth_token, user_id) do
    GenServer.call(__MODULE__, {:check_auth_token, auth_token, user_id})
  end
end
