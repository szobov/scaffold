defmodule Investigator.OAuth do
  use OAuth2.Strategy

  alias OAuth2.Strategy.AuthCode

  @config [
    strategy: __MODULE__,
    site: "https://api.github.com",
    authorize_url: "https://github.com/login/oauth/authorize",
    token_url: "https://github.com/login/oauth/access_token"
  ]

  @state_divider "<| |>"
  @auth_token_bytes_len 255

  @scope "read:user,repo,read:org"

  def client do
    [
      redirect_uri: System.get_env("GITHUB_REDIRECT_URL"),
      client_id: System.get_env("GITHUB_CLIENT_ID"),
      client_secret: System.get_env("GITHUB_CLIENT_SECRET")
    ]
    |> Keyword.merge(@config)
    |> OAuth2.Client.new()
  end

  defp generate_auth_token() do
    :crypto.strong_rand_bytes(@auth_token_bytes_len) |> Base.url_encode64()
  end

  defp generate_state(auth_token, user_id) do
    "#{user_id}#{@state_divider}#{auth_token}"
  end

  def extract_state(state) do
    case String.split(state, @state_divider) do
      [user_id, auth_token] -> {:ok, user_id, auth_token}
      _ -> {:error, :malformed}
    end
  end

  def authorize_url!(user_id) do
    auth_token = generate_auth_token()
    Investigator.Store.set_auth_token(auth_token, user_id)
    OAuth2.Client.authorize_url!(client(), scope: @scope, state: generate_state(auth_token, user_id))
  end

  def get_token!(params \\ [], headers \\ []) do
    OAuth2.Client.get_token!(client(), params, headers)
  end

  def authorize_url(client, params) do
    AuthCode.authorize_url(client, params)
  end

  def get_token(client, params, headers) do
    client
    |> put_param(:client_secret, client.client_secret)
    |> put_header("Accept", "application/json")
    |> AuthCode.get_token(params, headers)
  end
end
