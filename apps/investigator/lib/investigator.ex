defmodule Investigator do
  @moduledoc """
  Documentation for Investigator.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Investigator.hello
      :world

  """
  def hello do
    :world
  end
end
