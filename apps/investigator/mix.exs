defmodule Investigator.MixProject do
  use Mix.Project

  def project do
    [
      app: :investigator,
      version: "0.1.0",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      aliases: aliases()
    ]
  end

  defp aliases do
    [
      test: "test --no-start"
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :cowboy, :plug],
      mod: {Investigator.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:oauth2, "~> 0.9"},
      {:poison, "~> 3.1"},
      {:plug, "~> 1.5"},
      {:plug_cowboy, "~> 2.0"},
      {:exsync, "~> 0.1", only: :dev}
    ]
  end
end
