defmodule Gallows do
  @moduledoc """
  Documentation for Gallows.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Gallows.hello
      :world

  """
  def hello do
    :world
  end
end
