defmodule Gallows.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      {
        Gallows.Store,
        []
      },
      {
        Gallows.PostRequestProcessor,
        []
      },
      {
        Plug.Adapters.Cowboy,
        # XXX: If want to use https don't forget add appropriately keys
        # https://goo.gl/Ch6Sm6
        scheme: :http, plug: Gallows.Router, options: [port: 8100]
      }
      # Starts a worker by calling: Gallows.Worker.start_link(arg)
      # {Gallows.Worker, arg},
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Gallows.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
