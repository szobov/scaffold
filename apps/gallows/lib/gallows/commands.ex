defmodule Gallows.Commands do
  require Logger
  alias Gallows.Answers, as: Answers
  import String, only: [split: 1]

  def execute(["auth"], opts) do
    case Investigator.Store.get(opts.user_id) do
      {:ok, _} ->
        Answers.auth_already()

      {:error, _} ->
        opts.user_id |> Investigator.OAuth.authorize_url!() |> Answers.auth_first()
    end
  end

  def execute(["settings"], opts) do
    # TODO: refactor to better authorization check
    case opts.gh_token do
      "" -> execute(["auth"], opts)
      _ -> Answers.settings_buttons()
    end
  end

  def execute(["help"], _) do
    Answers.help()
  end

  def execute(_, opts) do
    # TODO: refactor to better authorization check
    case opts.gh_token do
      "" ->
        Logger.info("User <#{opts.user_id}> hasn't authorized yet")
        execute(["auth"], opts)

      _ ->
        Gallows.Actions.show_repos_prs(opts)
    end
  end

  def parse(command) do
    command
    |> split
  end
end
