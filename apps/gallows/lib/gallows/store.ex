defmodule Gallows.Store do
  use GenServer

  defmodule UserData do
    defstruct [:user_id, :repos]
  end

  def start_link(_) do
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(:ok) do
    {:ok, dets_storage} =
      :dets.open_file(
        :gallows_storage,
        [{:type, :set}, {:ram_file, true}]
      )

    {:ok, %{:storage => dets_storage}}
  end

  defp get_updated_repos(repos, repo) do
    case Enum.member?(repos, repo) do
      true ->
        repos -- [repo]

      false ->
        repos ++ [repo]
    end
  end

  defp get_user_repos(storage, user_id) do
    case :dets.lookup(storage, user_id) do
      [{_user_id, repos}] -> repos
      _ -> []
    end
  end

  def handle_call({:update_user_repos, user_id, repo}, _from, %{:storage => storage} = state) do
    updated_repos =
      storage
      |> get_user_repos(user_id)
      |> get_updated_repos(repo)

    :dets.insert(storage, {user_id, updated_repos})
    {:reply, {:ok, updated_repos}, state}
  end

  def handle_call({:get_user_repos, user_id}, _from, %{:storage => storage} = state) do
    {:reply, {:ok, get_user_repos(storage, user_id)}, state}
  end

  def update_user_repos(user_id, repo) do
    GenServer.call(__MODULE__, {:update_user_repos, user_id, repo})
  end

  def get_user_repos(user_id) do
    GenServer.call(__MODULE__, {:get_user_repos, user_id})
  end
end
