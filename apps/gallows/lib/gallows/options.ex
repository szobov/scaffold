defmodule Gallows.Options do
  @enforce_keys [:gh_token, :user_id]
  defstruct [:gh_token, :user_id]

  defimpl String.Chars, for: Gallows.Options do
    def to_string(opt) do
      "<gh_token: \"#{String.slice(opt.gh_token, 0..3)}...\", user_id: \"#{opt.user_id}\">"
    end
  end
end
