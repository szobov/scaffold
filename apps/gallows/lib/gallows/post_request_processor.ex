defmodule Gallows.PostRequestProcessor do
  require Logger
  use GenServer

  def init(args) do
    {:ok, args}
  end

  def start_link(_) do
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  defp send_request(url, body) do
    case HTTPoison.post(
           url,
           body,
           [{"Content-Type", "application/json"}]
         ) do
      {:ok, data} ->
        case data.status_code do
          200 ->
            Logger.debug(fn -> "Successfully call '#{url}'. Response: '#{data.body}'" end)

          err_code ->
            Logger.error(fn ->
              "Successfully call '#{url}', but receive error: '#{err_code}'. Response: '#{
                data.body
              }'"
            end)
        end

      {:error, err} ->
        Logger.debug(fn -> "Error on call '#{url}'. Error: '#{err.reason}'" end)
    end
  end

  def handle_cast({:command, text, response_url, opts}, state) do
    Logger.info(fn ->
      "Try to parse and process command from text '#{text}' for user '#{opts}'"
    end)

    result =
      text
      |> Gallows.Commands.parse()
      |> Gallows.Commands.execute(opts)

    Logger.debug(fn ->
      "Send answer to slack for user #{opts}: #{result}"
    end)

    send_request(response_url, result)
    Logger.info(fn -> "Answer has been sent to user #{opts.user_id}" end)
    {:noreply, state}
  end

  def handle_cast({:action, action, response_url, opts}, state) do
    Logger.info(fn ->
      "Try to process action '#{action["name"]}' for user '#{opts}'"
    end)

    result = Gallows.Actions.process(action, opts)
    send_request(response_url, result)
    Logger.info(fn -> "Answer has been sent to user #{opts.user_id}" end)
    {:noreply, state}
  end

  def command(text, response_url, opts) do
    GenServer.cast(__MODULE__, {:command, text, response_url, opts})
  end

  def action(action, response_url, opts) do
    GenServer.cast(__MODULE__, {:action, action, response_url, opts})
  end
end
