defmodule Gallows.Utils do
  def extract_full_repo_name(full) do
    String.split(full, "/")
  end
end
