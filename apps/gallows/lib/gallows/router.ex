defmodule Gallows.Router do
  require Logger

  use Plug.Router
  use Plug.ErrorHandler

  import Plug.Conn, only: [send_resp: 3, put_resp_content_type: 2]
  alias Gallows.Options, as: Options
  alias Gallows.PostRequestProcessor, as: PostRequestProcessor

  plug(:match)
  plug(Plug.Logger, log: Application.get_env(:logger, :level))

  plug(Plug.Parsers,
    parsers: [:urlencoded],
    pass: ["application/x-www-form-urlencoded"]
  )

  plug(:dispatch)

  defp get_slack_token do
    System.get_env("SLACK_API_TOKEN")
  end

  defp send_resp(conn, text \\ ":white_small_square:") do
    conn
    |> put_resp_content_type("application/json")
    |> send_resp(200, Poison.encode!(%{"text" => text, "replace_original" => true}))
  end

  post "/slack/scaffold" do
    Logger.info("Accept new request for command")

    case conn.body_params do
      %{
        "token" => slack_token,
        "command" => "/scaffold",
        "user_id" => user_id,
        "text" => text,
        "response_url" => response_url
      } ->
        case slack_token == get_slack_token() do
          true ->
            # TODO: refactor to better authorization check
            opts =
              case Investigator.Store.get(user_id) do
                {:ok, gh_token} ->
                  %Options{gh_token: gh_token, user_id: user_id}

                {:error, _} ->
                  %Options{gh_token: "", user_id: user_id}
              end

            PostRequestProcessor.command(text, response_url, opts)
            send_resp(conn, " ")

          false ->
            send_resp(conn, 401, "Unexpected slack-token")
        end

      _ ->
        send_resp(conn, 400, "Messing url-encoded body")
    end
  end

  post "/slack/scaffold-actions" do
    Logger.info("Accept new request for action")

    case conn.body_params do
      %{"payload" => json_data} ->
        case Poison.decode(json_data) do
          {:ok,
           %{
             "token" => slack_token,
             "type" => "interactive_message",
             "user" => %{"id" => user_id},
             "actions" => [action],
             "response_url" => response_url
           }} ->
            case slack_token == get_slack_token() do
              true ->
                {:ok, gh_token} = Investigator.Store.get(user_id)
                opts = %Options{gh_token: gh_token, user_id: user_id}
                PostRequestProcessor.action(action, response_url, opts)
                send_resp(conn)

              false ->
                send_resp(conn, 401, "Unexpected slack-token")
            end

          {:error, reason} ->
            IO.inspect(reason)
            send_resp(conn, 400, "Can't parse payload")
        end

      _ ->
        send_resp(conn, 400, "Invalid request")
    end
  end

  match _ do
    send_resp(conn, 404, "oops!")
  end

  def handle_errors(conn, %{kind: _kind, reason: _reason, stack: _stack}) do
    send_resp(conn, 500, "Something went wrong")
  end
end
