defmodule Gallows.Answers do
  require EEx

  @pagination_button_selector "_pagination_"
  @pr_colors %{:approved => "#D8E500", :your_own => "#CCC5C5", :need_approve => "#4DF047"}

  @sec_in_day 86400

  defp answer(text, attach) when is_list(attach) do
    Poison.encode!(%{"text" => text, "attachments" => attach})
  end

  defp answer(text, attach) when is_map(attach) do
    answer(text, [attach])
  end

  defp answer(text) when is_binary(text) do
    Poison.encode!(%{"text" => text})
  end

  defp answer(attach) when is_list(attach) do
    Poison.encode!(%{"attachments" => attach})
  end

  def auth_first(url) do
    """
    Hi! I'm scaffold bot. I will help you to review your github PRs.
    If you want to work with me, authorize us in your github account using this
    link: <#{url}|github.com>.
    """
    |> answer
  end

  def auth_already do
    """
    Unfortenutely you can authorize only once.
    """
    |> answer
  end

  defp button(name, text, value, style \\ "default") do
    %{
      "name" => name,
      "text" => text,
      "type" => "button",
      "value" => value,
      "style" => style
    }
  end

  def settings_buttons do
    text = "Configure the list of the interested repositories"

    attach = %{
      "text" => "Choose:",
      "callback_id" => "settings_buttons",
      "attachment_type" => "default",
      "actions" => [
        button("selected_repos", "Selected repositories", 1, "primary"),
        button("organizations", "Organizations repositories", 1),
        # button("user_repos", "Your repositories", 1),
        button("show_repos_prs", "Let's review!", 1, "danger")
      ]
    }

    answer(text, attach)
  end

  def help do
    [
      %{
        "pretext" => """
        The command supports the arguments:
        *auth*, *settings*, *help*
        or you can invoke it without the arguments.
        """
      },
      %{
        "title" => "auth",
        "text" => "Prepares a link for a github authorization."
      },
      %{
        "title" => "settings",
        "text" => """
        Contains the buttons for configuring repositories.
        You have to select interesting repositories at first.
        """
      },
      %{
        "title" => "without arguments",
        "text" => """
        It shows you all open pull requests for the selected repositories.
        It uses the colored legend to mark PRs in different states:
        """
      },
      %{
        "text" => "likely you have to review and approve this",
        "color" => @pr_colors.need_approve
      },
      %{"text" => "you've already approved this", "color" => @pr_colors.approved},
      %{"text" => "it's your own", "color" => @pr_colors.your_own}
    ]
    |> answer
  end

  def user_repos do
    "Not implemented yet!"
    |> answer(return_back_button("settings"))
  end

  def user_organizations(orgs) do
    attach = %{
      "text" => "Show organization repos",
      "callback_id" => "show_org_repos",
      "attachment_type" => "default",
      "actions" =>
        Enum.map(
          orgs,
          fn info -> button("user_org_repos", info["name"], info["login"]) end
        )
    }

    answer(return_back_button("settings") ++ [attach])
  end

  def selected_repos(selected_repos) do
    attach_selected = prepare_selected_repos(selected_repos)
    answer(return_back_button("settings") ++ attach_selected)
  end

  defp return_back_button(selector) do
    [
      %{
        "attachment_type" => "default",
        "callback_id" => "return_back_settings",
        "fallback" => "oops!",
        "actions" => [button(selector, "Return back", 1, "primary")]
      }
    ]
  end

  defp prepare_selected_repos(repos) do
    case repos do
      [] ->
        [
          %{
            "text" => "You don't have any selected repositories. Return back to select some."
          }
        ]

      _ ->
        [
          %{
            "title" => "Selected repos:",
            "text" =>
              EEx.eval_string(
                """
                <%= for repo <- selected_repos do %>
                _<%= repo %>_
                <% end %>
                """,
                selected_repos: repos
              )
          }
        ]
    end
  end

  defp option(text, value) do
    %{
      "text" => text,
      "value" => value
    }
  end

  def user_organization_repos(repos, org_login, selected_repos \\ []) do
    attach_selected = prepare_selected_repos(selected_repos)

    attach_menu = [
      %{
        "text" => "Add repo",
        "callback_id" => "show_org_repos",
        "attachment_type" => "default",
        "actions" =>
          [
            %{
              "name" => "user_selected_repos",
              "type" => "select",
              "text" => "Choose repo",
              "options" =>
                Enum.map(
                  repos,
                  fn info -> option(info["name"], "#{org_login}/#{info["name"]}") end
                )
            }
          ] ++ [button("organizations", "Return back", 1)]
      }
    ]

    answer(attach_selected ++ attach_menu)
  end

  def make_paginations_buttons(pagination, options) do
    make_value = fn cursor_name ->
      Poison.encode!(Map.put(options, "cursor", pagination[cursor_name]))
    end

    match = %{
      "hasPreviousPage" => {"startCursor", "<<"},
      "hasNextPage" => {"endCursor", ">>"}
    }

    for has <- Map.keys(match),
        pagination[has] == true,
        {cursor_name, arrow} = match[has] do
      button(@pagination_button_selector, arrow, make_value.(cursor_name))
    end
  end

  def there_are_not_selected_repos do
    """
    It's better idea to select interesting repositories at first.
    Use parameter `settings` for this.
    """
    |> answer
  end

  def show_repos_prs(info) do
    {%{"login" => current_user}, prs_info} = Map.pop(info, "current_user")

    for i <- Map.values(prs_info),
        repo_name = i["nameWithOwner"],
        prs = get_in(i, ["pullRequests", "nodes"]),
        length(prs) != 0 do
      [
        %{"pretext" => "=== *#{repo_name}* ==="}
        | prepare_prs(prs, current_user)
      ]
    end
    |> List.flatten()
    |> check_show_repos_pr_empty
    |> answer
  end

  defp check_show_repos_pr_empty(prs) do
    case prs do
      [] ->
        "*Nothing to review.*\nWell done! :+1:"

      _ ->
        prs
    end
  end

  defp prepare_prs(prs, current_user) do
    prepare_prs(prs, current_user, [])
  end

  defp prepare_prs([], _, acc) do
    acc
  end

  defp prepare_prs(prs, current_user, acc) do
    [pr | rest] = prs

    case current_user == pr["author"]["login"] do
      true ->
        prepare_prs(
          rest,
          current_user,
          acc ++ [prepare_pr(pr, current_user, @pr_colors.your_own)]
        )

      false ->
        prepare_prs(
          rest,
          current_user,
          [prepare_pr(pr, current_user, @pr_colors.need_approve) | acc]
        )
    end
  end

  defp prepare_pr(pr, current_user, color) do
    {is_user_approved_pr, reviews} =
      prepare_reviews(
        pr["reviews"]["nodes"],
        make_review_requested_set(pr["reviewRequests"]["nodes"]),
        current_user
      )

    %{
      "title" => pr["title"],
      "title_link" => pr["url"],
      "text" => reviews,
      "footer" => "#{pr["author"]["login"]} | #{get_time_since_pr_created(pr["createdAt"])}",
      "footer_icon" => pr["author"]["avatarUrl"],
      "color" =>
        if is_user_approved_pr do
          @pr_colors.approved
        else
          color
        end
    }
  end

  defp get_time_since_pr_created(created_at) do
    {:ok, datetime, 0} = DateTime.from_iso8601(created_at)
    days_since_created = div(DateTime.diff(DateTime.utc_now(), datetime), @sec_in_day)

    case days_since_created >= 1 do
      false -> "created less than day ago"
      true -> "created days ago: #{days_since_created}"
    end
  end

  defp make_review_requested_set(review_requests) do
    Enum.reduce(review_requests, MapSet.new(), fn x, acc ->
      MapSet.put(acc, x["requestedReviewer"]["login"])
    end)
  end

  defp prepare_reviews(reviews, review_requests_set, current_user) when is_binary(current_user) do
    res = prepare_reviews(reviews, review_requests_set, %{})

    f = fn selector ->
      Enum.join(
        for r <- Map.values(res), r["state"] == selector do
          r["author"]["login"]
        end,
        ", "
      )
    end

    approved = f.("APPROVED")
    changes_requested = f.("CHANGES_REQUESTED")

    res =
      case approved do
        "" -> ""
        _ -> ":heavy_check_mark:#{approved}"
      end

    res =
      case changes_requested do
        "" -> res
        _ -> "#{res}\n:x:#{changes_requested}"
      end

    {String.contains?(approved, current_user), res}
  end

  defp prepare_reviews([], _, acc) do
    acc
  end

  defp prepare_reviews(reviews, review_requests_set, acc) do
    [r | rest] = reviews
    review_author = r["author"]["login"]

    prepare_reviews(
      rest,
      review_requests_set,
      case MapSet.member?(review_requests_set, review_author) do
        true -> acc
        false -> check_most_recent_review(review_author, r, acc)
      end
    )
  end

  defp check_most_recent_review(review_author, review, review_acc) do
    case Map.get(review_acc, review_author) do
      nil ->
        Map.put(review_acc, review_author, review)

      checked_review ->
        case checked_review['publishedAt'] > review['publishedAt'] do
          true -> review_acc
          false -> Map.put(review_acc, review_author, review)
        end
    end
  end
end
