defmodule Gallows.Api do
  require Logger

  @endpoint "https://api.github.com/graphql"
  @batch 10
  @pagination_fragment """
  pageInfo {
     startCursor
     hasPreviousPage
     endCursor
     hasNextPage
  }
  """

  defp make_after_fragment(cursor) do
    case cursor do
      nil -> ""
      _ -> ", after: \"#{cursor}\""
    end
  end

  defp prepare_headers(token) do
    headers = [
      {"Authorization", "bearer #{token}"},
      {"Content-Type", "application/graphql"}
    ]

    headers
  end

  defp request!(query, token) do
    HTTPoison.post!(
      @endpoint,
      query,
      prepare_headers(token)
    )
  end

  defp decode_response!(response) do
    response.body
    |> Poison.decode!()
  end

  defp prepare_query!(query) do
    Poison.encode!(%{"query" => query})
  end

  defp execute!(query, token) do
    Logger.debug(fn -> "Make the request to GitHub api, using: #{query}" end)

    res =
      query
      |> prepare_query!
      |> request!(token)
      |> decode_response!

    Logger.debug(fn -> "Received the answer #{inspect(res)}" end)
    res
  end

  def get_user_repos(token, cursor \\ nil, count \\ @batch) do
    """
    query {
      viewer {
        repositories(first: #{count} #{make_after_fragment(cursor)}) {
          nodes {
            name
          }
          #{@pagination_fragment}
        }
      }
    }
    """
    |> execute!(token)
    |> get_in(["data", "viewer", "repositories"])
  end

  def get_user_organizations(token, cursor \\ nil, count \\ @batch) do
    """
    query {
      viewer {
      organizations(first: #{count} #{make_after_fragment(cursor)}) {
          nodes {
            name
            login
          }
          #{@pagination_fragment}
        }
      }
    }
    """
    |> execute!(token)
    |> get_in(["data", "viewer", "organizations"])
  end

  def get_organization_repos(token, login, cursor \\ nil, count \\ @batch) do
    """
    query {
      viewer {
        organization(login: "#{login}") {
          repositories(first: #{count} #{make_after_fragment(cursor)}){
            nodes {
              name
            }
            #{@pagination_fragment}
          }
        }
      }
    }
    """
    |> execute!(token)
    |> get_in(["data", "viewer", "organization", "repositories"])
  end

  def get_repo_pr(token, owner, repo_name, cursor \\ nil, count \\ @batch) do
    """
    query {
       repository(owner: "#{owner}", name: "#{repo_name}"){
          pullRequests(first: #{count}, states:OPEN, #{make_after_fragment(cursor)}){
            nodes {
              title
              url
              author {
                 login
              }
              reviews(first: #{count}, states:APPROVED){
                nodes {
                  author {
                    login
                  }
                }
              }
            }
            #{@pagination_fragment}
          }
      }
    }
    """
    |> execute!(token)
    |> get_in(["data", "repository", "pullRequests"])
  end

  def get_repos_prs_and_current_user(list_of_repos, token) do
    EEx.eval_string(
      """
      query {
      current_user: viewer {login}
      <%= for {full_name, i} <- Enum.with_index(list_of_repos),
              [owner, name] = Gallows.Utils.extract_full_repo_name(full_name)
              do %>
      query<%= i %>:
        repository(owner: "<%= owner %>", name: "<%= name %>"){
          nameWithOwner
          pullRequests(first: 40, states:OPEN){
            nodes {
              title
              url
              createdAt
              author {
                login
                avatarUrl(size:16)
              }
              reviewRequests(first: 40){
                nodes {
                  requestedReviewer {
                     ... on User {
                       login
                     }
                  }
                }
              }
              reviews(first: 40, states:[APPROVED,CHANGES_REQUESTED]){
                nodes {
                  state
                  publishedAt
                  author {
                    login
                  }
                }
              }
            }
          }
        }
      <% end %>
      }
      """,
      list_of_repos: list_of_repos
    )
    |> execute!(token)
    |> get_in(["data"])
  end
end
