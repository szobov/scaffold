defmodule Gallows.Actions do
  require Logger
  alias Gallows.Api, as: Api
  alias Gallows.Answers, as: Answers
  alias Gallows.Store, as: Store
  alias Gallows.Utils, as: Utils

  def process(%{"name" => "organizations"}, opts) do
    Api.get_user_organizations(opts.gh_token)["nodes"]
    |> Answers.user_organizations()
  end

  def process(%{"name" => "settings"}, _opts) do
    Answers.settings_buttons()
  end

  def process(%{"name" => "selected_repos"}, opts) do
    get_user_selected_repos(opts.user_id)
    |> Answers.selected_repos()
  end

  def process(%{"name" => "user_repos"}, _opts) do
    Answers.user_repos()
  end

  def process(%{"name" => "user_org_repos", "value" => organization}, opts) do
    selected_repos = get_user_selected_repos(opts.user_id)

    get_all_organization_repos(opts.gh_token, organization)
    |> Answers.user_organization_repos(organization, selected_repos)
  end

  def process(act = %{"name" => "user_selected_repos"}, opts) do
    case Map.delete(act, "name") do
      %{"selected_options" => [%{"value" => full_name}]} ->
        selected_repos = update_user_repos(opts, full_name)
        [org, _] = Utils.extract_full_repo_name(full_name)

        get_all_organization_repos(opts.gh_token, org)
        |> Answers.user_organization_repos(org, selected_repos)
    end
  end

  def process(%{"name" => "show_repos_prs"}, opts) do
    show_repos_prs(opts)
  end

  def update_user_repos(opts, full_repo_name) do
    {:ok, repos} = Store.update_user_repos(opts.user_id, full_repo_name)
    repos
  end

  defp get_user_selected_repos(user_id) do
    {:ok, selected_repos} = Store.get_user_repos(user_id)
    selected_repos
  end

  defp get_all_organization_repos(gh_token, org) do
    res = Api.get_organization_repos(gh_token, org)

    get_all_organization_repos(
      gh_token,
      org,
      res["pageInfo"]["hasNextPage"],
      res["pageInfo"]["endCursor"],
      res["nodes"]
    )
  end

  defp get_all_organization_repos(_, _, false, _, repos_acc) do
    repos_acc
  end

  defp get_all_organization_repos(gh_token, org, true, cursor, repos_acc) do
    res = Api.get_organization_repos(gh_token, org, cursor, 40)

    get_all_organization_repos(
      gh_token,
      org,
      res["pageInfo"]["hasNextPage"],
      res["pageInfo"]["endCursor"],
      res["nodes"] ++ repos_acc
    )
  end

  def show_repos_prs(opts) do
    case get_user_selected_repos(opts.user_id) do
      [] ->
        Answers.there_are_not_selected_repos()

      info ->
        Logger.debug(fn -> "User selected repos: #{inspect(info)}" end)

        info
        |> Api.get_repos_prs_and_current_user(opts.gh_token)
        |> Answers.show_repos_prs()
    end
  end
end
