defmodule GallowsRouterTest do
  use ExUnit.Case
  use Plug.Test

  @opts Gallows.Router.init([])

  defp check_connection_was_sent(conn, status, body) do
    assert conn.state == :sent
    assert conn.status == status
    assert conn.resp_body == body
  end

  @tag :skip
  test "check POST /slack/scaffold" do
    params = %{token: "FooBar", command: "/scaffold help", baz: "bar"}

    conn(:post, "/slack/scaffold", params)
    |> put_req_header("content-type", "application/x-www-form-urlencoded")
    |> Gallows.Router.call(@opts)
    |> check_connection_was_sent(200, "scaffold_right_answer")
  end

  @tag :skip
  test "check wrong command POST /slack/scaffold" do
    params = %{token: "foo", command: "/scarface help", baz: "bar"}

    conn(:post, "/slack/scaffold", params)
    |> put_req_header("content-type", "application/x-www-form-urlencoded")
    |> Gallows.Router.call(@opts)
    |> check_connection_was_sent(400, "Wrong command")
  end

  @tag :skip
  test "check wrong body POST /slack/scaffold" do
    conn(:post, "/slack/scaffold")
    |> Gallows.Router.call(@opts)
    |> check_connection_was_sent(400, "Messing urlencoded body")
  end
end
