defmodule AnswersTest do
  use ExUnit.Case
  import Gallows.Answers

  defp button(name, text, value) do
    %{
      "name" => name,
      "text" => text,
      "type" => "button",
      "value" => value
    }
  end

  test "Check pagination buttons maker" do
    pagination_fragment = %{
      "startCursor" => "=StArT",
      "hasPreviousPage" => true,
      "endCursor" => "=EnD",
      "hasNextPage" => true
    }

    options = %{"name" => "something_interesting", "options" => %{"foo" => 1}}

    expected = [
      button(
        "_pagination_",
        ">>",
        Poison.encode!(%{
          "cursor" => "=EnD",
          "name" => "something_interesting",
          "options" => %{"foo" => 1}
        })
      ),
      button(
        "_pagination_",
        "<<",
        Poison.encode!(%{
          "cursor" => "=StArT",
          "name" => "something_interesting",
          "options" => %{"foo" => 1}
        })
      )
    ]

    assert make_paginations_buttons(pagination_fragment, options) == expected
  end
end
