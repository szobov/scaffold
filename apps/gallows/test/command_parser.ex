defmodule CommandParserTest do
  use ExUnit.Case
  import Gallows.CommandParser, only: [parse: 1]

  defp check(raw, expected) do
    assert parse(raw) == expected
  end

  @tag :skip
  test "Parse raw command" do
    check("/fooo bar baz", ["bar", "baz"])
    check("/fooo", [])
    check("/fooo         bar                ", ["bar"])
  end
end
