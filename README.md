# Scaffold

It's a [slack-bot](https://api.slack.com/bot-users) that helps you to review the pull requests on [GitHub](https://github.com/).

Currently, it supports only PR in your organizations repositories.

# Disclaimer

I've made this bot for my colleagues. Also, that was my first time of writing something with [Elixir](https://elixir-lang.org/), so don't treat me strictly.

# How to use it

You can run your own instance of this bot by following this steps:

1. Create [OAuth App](https://help.github.com/en/articles/authorizing-oauth-apps) on GitHub.
2. You'll need to save *Client ID* and *Client Secret*.
3. Prepare a server, that can server some domain-name, like foobar.com, and redirect traffic to the bot application. You can use [this](https://gitlab.com/szobov/scaffold/raw/master/haproxy.cfg) [haproxy](http://www.haproxy.org/) config example.
4. After, you have to add *Authorization callback URL* to your *OAuth App* from the first step. It will look like __https://foobar.com/investigator/auth/success__.
5. Create [slack app](https://api.slack.com/tutorials) and save *Verification Token*. Add a custom command and *Request URL* for it: __https://foobar.com/gallows/slack/scaffold__. Also add *Interactive Components* with *Request URL* like __https://foobar.com/gallows/slack/scaffold-actions__.
6. [Install elixir](https://elixir-lang.org/install.html) on your server. Prepare also [systemd](https://freedesktop.org/wiki/Software/systemd/). You can use [docker](https://www.docker.com/get-started) container instead.
7. [Make a release](https://elixir-lang.org/getting-started/mix-otp/config-and-releases.html) of the bot application. You can do it with command `MIX_ENV=prod mix release`. Copy release's `_build` directory to your server. (If you will successfully made the release and it will work perfectly you don't need to install elixir on your server at step 6).
8. Before the start you have to prepare a file with environment variables and export it. Something like this:

```
export GITHUB_CLIENT_ID=blahblah
export GITHUB_CLIENT_SECRET=blahblag
export GITHUB_REDIRECT_URL=https://foobar.com/investigator/auth/success
export SLACK_API_TOKEN=blahblah
```

9. Start the bot-application, using the executable files from the release, or configure systemd-service, using [this example](https://gitlab.com/szobov/scaffold/raw/master/scaffold.service). 

# Developing

If you decide to test or contribute, I really suggest you to use [ngrok](https://ngrok.com/) to provide link for *slack app*. Start *ngrok* with command `ngrok http 8100` and copy a generated link to the interface from step 5. It will look like __http://554075ad.ngrok.io/slack/scaffold-actions__.

The bot-application is elixir [umbrella application](https://elixir-lang.org/getting-started/mix-otp/dependencies-and-umbrella-projects.html), that contains two applications: [gallows](https://gitlab.com/szobov/scaffold/tree/master/apps/gallows) and [investigator](https://gitlab.com/szobov/scaffold/tree/master/apps/investigator). `Investigator` is responsible for github authentication and `gallows` for all other features.
